# blog-with-submodules

blog in Spring Boot and Angular

to clone and fetch the submodules at the same time:
`git clone --recurse-submodules`

If you already cloned the project and forgot `--recurse-submodules`, run `git submodule update --init`

## blog-backend

API backend using Spring Boot and JDBC

## blog-frontend

Responsive frontend using Angular

